package com.mirror.component;

import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author shenggangfeng
 * @description
 * @date 2021-02-23 14:20
 */
@Component
@Order(2)
public class MyCommandLine implements CommandLineRunner {

    @Override
    public void run(String... args) throws Exception {
        System.out.println("回调CommandLineRunner...");
    }

}

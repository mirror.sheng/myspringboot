package com.mirror.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author shenggangfeng
 * @description
 * @date 2021-02-23 16:42
 */
@Component
@ConfigurationProperties("my")
public class PropertyListConfig {

    private List<String> server;

    public List<String> getServer() {
        return server;
    }

    public void setServer(List<String> server) {
        this.server = server;
    }
}

package com.mirror;

import com.mirror.config.PropertyListConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PreDestroy;
import java.util.Arrays;
import java.util.List;

/**
 * @author shenggangfeng
 * @description
 * @date 2021-02-22 14:51
 */
@RestController
@SpringBootApplication
public class MySpringBoot {

    @Value("${name}")
    private String name;
    @Value("${password}")
    private String password;
    @Value("${description}")
    private String description;
    @Value("${my.list}")
    private String[] myArray;
    @Value("#{'${my.list}'.split(',')}")
    private List<String> myList;
    @Value("${my.uuid}")
    private String uuid;
    @Autowired
    private PropertyListConfig propertyListConfig;

    @RequestMapping("/a")
    public String home() {
        return name + password;
    }

    @RequestMapping("/b")
    public String library() {
        return description;
    }

    @RequestMapping("/c")
    public String family() {
        return propertyListConfig.getServer().get(0) + " love " + propertyListConfig.getServer().get(1);
    }

    @RequestMapping("/d")
    public String age() {
        return Arrays.toString(myArray);
    }

    @RequestMapping("/e")
    public String money() {
        return myList.toString();
    }

    @RequestMapping("/uuid")
    public String uuid() {
        return uuid;
    }


    public static void main(String[] args) {

        SpringApplication.run(MySpringBoot.class, args);

        // 以下为自定义SpringApplication
//        SpringApplication application = new SpringApplication(MySpringBoot.class);
//        // 关闭banner
//        application.setBannerMode(Banner.Mode.OFF);
//        application.run(args);
    }

    @PreDestroy
    public void preDestroy() {
        System.out.println("我销毁了...");
    }

}
